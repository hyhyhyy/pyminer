import os
import sys
from typing import Tuple, List, Dict, Union, TYPE_CHECKING
import json
from PyQt5.QtWidgets import QGraphicsTextItem, QGraphicsPixmapItem
from PyQt5.QtCore import QObject, QPointF
from PyQt5.QtGui import QPixmap

from pmgwidgets.flowchart.flow_items import CustomRect, CustomPort
from pmgwidgets import get_parent_path

if TYPE_CHECKING:
    from pmgwidgets.flowchart.flowchart_widget import PMGraphicsScene
from pmgwidgets.flowchart.flow_content import FlowContent


class Node(QObject):
    """
    Node是一个节点的抽象
    """

    def __init__(self, canvas: 'PMGraphicsScene', node_id, text: str = '', input_ports: List[CustomPort] = None,
                 output_ports: List[CustomPort] = None, content: object = None, icon_path=r''):
        super(Node, self).__init__()

        self.id = node_id
        self.text = text if text != '' else node_id
        self.base_rect = CustomRect(self)
        self.text_item = QGraphicsTextItem(parent=self.base_rect)
        self.text_item.setPos(80, 10)
        self.text_item.setPlainText(self.text)

        self.status_text = QGraphicsTextItem(parent=self.base_rect)
        self.status_text.setPos(80, 100)
        self.status_text.setPlainText('wait')

        icon_path = os.path.join(get_parent_path(__file__), 'icons', 'logo.png') if icon_path == '' else icon_path
        self.icon_path = icon_path
        pix_map = QPixmap(icon_path)
        self.pix_map_item = QGraphicsPixmapItem(pix_map, parent=self.base_rect)
        self.pix_map_item.setPos(80, 30)

        self.input_ports = input_ports
        self.output_ports = output_ports
        self.canvas = canvas
        self.set_content(content)
        self.setup()

    def set_content(self, content: 'FlowContent'):
        self.content: 'FlowContent' = content if content is not None else FlowContent(self, '')
        self.content.signal_exec_finished.connect(self.on_exec_finished)
        self.content.signal_exec_started.connect(self.on_exec_started)

    def on_exec_started(self, content):
        """
        执行前进行的操作
        :param content:
        :return:
        """
        print('on_exec_finished')
        self.status_text.setPlainText(content)

    def on_exec_finished(self, content: str):
        """
        执行完成后进行的操作
        :param content:
        :return:
        """
        print('on_exec_finished')
        self.status_text.setPlainText(content)

    #
    def reset(self):
        self.status_text.setPlainText('wait')

    def get_port_index(self, port: CustomPort):
        """
        获取端口的索引
        :param port:
        :return:
        """
        return self.input_ports.index(port) if port.port_type == 'input' else self.output_ports.index(port)

    def set_icon(self, icon_path: str):
        pass

    def set_pos(self, x: int, y: int):
        """
        设置位置，左上角角点
        :param x:
        :param y:
        :return:
        """
        self.base_rect.setPos(x, y)
        self.refresh_pos()

    def get_pos(self) -> Tuple[int, int]:
        """
        获取位置，左上角角点
        :return:
        """
        pos = self.base_rect.pos()
        return pos.x(), pos.y()

    def refresh_pos(self):
        y = self.base_rect.y()
        dy_input = self.base_rect.boundingRect().height() / (1 + len(self.input_ports))
        dy_output = self.base_rect.boundingRect().height() / (1 + len(self.output_ports))
        x_input = self.base_rect.x() + 5
        x_output = self.base_rect.x() + self.base_rect.boundingRect().width() - 15
        for i, p in enumerate(self.input_ports):
            p.setPos(QPointF(x_input, y + int(dy_input * (1 + i))))
        for i, p in enumerate(self.output_ports):
            p.setPos(QPointF(x_output, y + int(dy_output * (1 + i))))
        self.canvas.signal_item_dragged.emit('')

    def setup(self):
        self.base_rect.setPos(80, 80)
        self.canvas.signal_clear_selection.connect(self.base_rect.on_clear_selection)
        self.canvas.addItem(self.base_rect)

        for p in self.input_ports + self.output_ports:
            self.canvas.addItem(p)
            p.port_clicked.connect(self.on_port_clicked)
            p.node = self
        self.refresh_pos()

    def on_port_clicked(self, port: 'CustomPort'):
        if self.canvas.drawing_lines:
            if self.canvas.line_start_port is not port:
                self.canvas.connect_port(port)
            else:
                self.canvas.drawing_lines = False
        else:
            self.canvas.drawing_lines = True
            self.canvas.line_start_point = port.center_pos
            self.canvas.line_start_port = port

    def on_delete(self):
        for port in self.input_ports + self.output_ports:
            port.canvas = self.canvas
            port.on_delete()
        self.canvas.removeItem(self.base_rect)
        self.canvas.nodes.remove(self)
        self.deleteLater()

    def __repr__(self):
        s = super(Node, self).__repr__()
        return s + repr(self.input_ports) + repr(self.output_ports)

    def get_port(self, port_id: str) -> 'CustomPort':
        for port in self.input_ports + self.output_ports:
            if port_id == port.id:
                return port
        return None

    def add_port(self, port: CustomPort):
        """
        添加一个端口
        :param port:
        :return:
        """

        port_type = port.port_type
        if port_type == 'input':
            self.input_ports.append(port)
        elif port_type == 'output':
            self.output_ports.append(port)
        else:
            raise ValueError('port type invalid')
        self.refresh_pos()
        self.canvas.addItem(port)
        port.port_clicked.connect(self.on_port_clicked)
        return port

    def remove_port(self, port: CustomPort):
        """
        删除一个端口
        :param port:
        :return:
        """

        port_type = port.port_type
        if port_type == 'input':
            self.input_ports.remove(port)
        elif port_type == 'output':
            self.output_ports.remove(port)
        else:
            raise ValueError('port type invalid')

        port.on_delete()
        self.refresh_pos()

    def change_property(self, property: Dict[str, Union[int, str]]):
        """
        改变各个端口的文字、端口的数目以及文字。
        :param property:
        :return:
        """
        self.text = property['text']
        self.text_item.setPlainText(self.text)
        self.content.update_settings(property)
        self.valid_port_ids = []
        for input_id, input_text in zip(property['inputs'][0], property['inputs'][1]):
            self.valid_port_ids.append(input_id)
            p = self.get_port(input_id)
            if p is None:
                p = self.add_port(CustomPort(port_id=input_id, text=input_text, port_type='input'))
            p.set_text(input_text)

        for output_id, output_text in zip(property['outputs'][0], property['outputs'][1]):
            self.valid_port_ids.append(output_id)
            p = self.get_port(output_id)
            if p is None:
                p = self.add_port(CustomPort(port_id=output_id, text=output_text, port_type='output'))
            p.set_text(output_text)

        for p in self.input_ports + self.output_ports:
            if p.id not in self.valid_port_ids:
                self.remove_port(p)

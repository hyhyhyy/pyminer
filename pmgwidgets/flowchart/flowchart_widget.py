import os
import sys
from typing import List, Dict
import json
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QToolButton, QSpacerItem, QSizePolicy, QGraphicsView, \
    QFrame, QApplication, QGraphicsScene, QGraphicsSceneMouseEvent, QMenu, QGraphicsSceneContextMenuEvent, QFileDialog
from PyQt5.QtCore import QSize, QCoreApplication, pyqtSignal, QLineF, Qt, QPointF, QThread
from PyQt5.QtGui import QPen, QColor, QKeyEvent, QWheelEvent

from pmgwidgets.flowchart.flow_items import CustomPort, CustomLine, CustomMidPoint
from pmgwidgets.flowchart.flow_node import Node
from pmgwidgets.flowchart.flow_content import FlowContent

COLOR_NORMAL = QColor(212, 227, 242)
COLOR_HOVER = QColor(255, 200, 00)
COLOR_HOVER_PORT = QColor(0, 0, 50)


class PMGraphicsScene(QGraphicsScene):
    signal_item_dragged = pyqtSignal(str)  # 拖拽控件发生的事件。
    signal_port_clicked = pyqtSignal(str)  # 点击端口的事件
    signal_clear_selection = pyqtSignal()  # 清除选择的事件

    def __init__(self, parent=None, graphics_view: 'QGraphicsView' = None, flow_widget: 'PMFlowWidget' = None):
        super().__init__(parent)
        self.call_id_list: List = []
        self.lines: List[CustomLine] = []
        self.nodes: List['Node'] = []
        self.drawing_lines = False
        self.line_start_port = None
        self.line_start_point = None
        self.line_end_port = None
        self.node_index_to_execute = 0
        self.line = self.addLine(0, 0, 1, 1, QPen())
        self.graphics_view = graphics_view
        self.flow_widget: 'PMFlowWidget' = flow_widget
        self.menu = QMenu()
        self.menu.addAction('New').triggered.connect(lambda x: self.add_node())
        self.menu.addAction('Delete Selected').triggered.connect(lambda x: self.delete_selected_item())
        self.selected_items = []

    def contextMenuEvent(self, event: 'QGraphicsSceneContextMenuEvent') -> None:
        super(PMGraphicsScene, self).contextMenuEvent(event)
        self.menu.exec_(event.screenPos())

    def keyPressEvent(self, event: 'QKeyEvent') -> None:
        if event.key() == Qt.Key_Delete:
            self.delete_selected_item()
        elif event.key() == Qt.Key_Escape:
            if self.drawing_lines:
                self.drawing_lines = False
                self.line.hide()

    def mouseMoveEvent(self, event: 'QGraphicsSceneMouseEvent') -> None:
        super(PMGraphicsScene, self).mouseMoveEvent(event)
        if self.drawing_lines:
            self.line.show()
            self.line_end_point = event.scenePos()
            self.line.setLine(QLineF(self.line_end_point, self.line_start_point))
        # print('mouse_moved')
        self.sceneRect()
        self.graphics_view.viewport().update()

    def connect_port(self, end_port):
        self.line.hide()
        line = CustomLine(self.line_start_port, end_port, self)
        self.lines.append(line)
        self.addItem(line)
        self.signal_item_dragged.connect(line.refresh)
        self.drawing_lines = False

    def find_port(self, port_id: int):
        for n in self.nodes:
            for p in n.input_ports + n.output_ports:
                if p.id == port_id:
                    return p
        return None

    def find_node(self, node_id: str) -> Node:
        for n in self.nodes:
            if n.id == node_id:
                return n
        return None

    def new_id(self) -> str:
        max_id = 0
        for n in self.nodes:
            a = int(n.id)
            if a > max_id:
                max_id = a
        new_id = max_id + 1
        return str(new_id)

    def add_node(self):
        view = self.graphics_view
        node_id = self.new_id()
        input_ports = [CustomPort(node_id + ':input:1'), CustomPort(node_id + ':input:2')]
        output_ports = [CustomPort(node_id + ':output:1', port_type='output'),
                        CustomPort(node_id + ':output:2', port_type='output'),
                        CustomPort(node_id + ':output:3', port_type='output')]
        n = Node(self, node_id, input_ports=input_ports, output_ports=output_ports)
        n.set_pos(200, 50)
        print(n.get_pos())
        self.nodes.append(n)

    def delete_selected_item(self):
        """
        删除被选中的物品
        :return:
        """
        print(self.selectedItems())
        for selected_item in self.selected_items:
            if hasattr(selected_item, 'on_delete'):
                selected_item.on_delete()
        self.selected_items = []
        print('delete!')

    def unselect_item(self, item):
        if item in self.selected_items:
            self.selected_items.remove(item)

    def select_item(self, item):
        if item not in self.selected_items:
            self.selected_items.append(item)

    def topo_sort(self, draw_graph=False):
        """
        拓扑排序
        :return:
        """
        length = len(self.nodes)
        start_node: Node = None
        temp_node_list = self.nodes.copy()
        sorted_list = []
        lines = []
        for l in self.lines:
            start_id, end_id = l.start_port.node.id, l.end_port.node.id
            lines.append((start_id, end_id))
        import networkx

        DG = networkx.DiGraph(lines)
        if draw_graph:
            networkx.draw_spring(DG, with_labels=True)
            import matplotlib.pyplot as plt
            plt.show()
        return list(networkx.topological_sort(DG))

    def load_flowchart(self, path=''):
        # self.reset()
        file = './examples/flowchart_stat.json' if path == '' else path
        print(file)
        if not os.path.exists(file):
            return
        with open(file, 'r') as f:
            text = f.read()
            fc_info_dic: Dict[str, Dict] = json.loads(text)
            nodes_dic = fc_info_dic['nodes']
            connections: List = fc_info_dic['connections']
        for k in nodes_dic.keys():
            node_property = nodes_dic[k]
            node_name = node_property['id']
            node_pos = node_property['pos']
            node_text = node_property['text']
            node_icon_path = node_property['icon']
            node_content = node_property['content']
            input_ports = []
            for input_port_id in node_property['input_ports'].keys():
                port_property = node_property['input_ports'][input_port_id]
                port = CustomPort(port_id=input_port_id, text=port_property['text'], content=port_property['contents'],
                                  port_type='input')
                input_ports.append(port)
            output_ports = []
            for output_port_id in node_property['output_ports'].keys():
                port_property = node_property['output_ports'][output_port_id]
                port = CustomPort(port_id=output_port_id, text=port_property['text'], content=port_property['contents'],
                                  port_type='output')
                output_ports.append(port)

            node = Node(self, node_name, text=node_text, input_ports=input_ports, output_ports=output_ports,
                        icon_path=node_icon_path)
            content = FlowContent(node=node, code=node_content['code'])
            node.set_content(content)
            node.set_pos(*node_pos)
            self.nodes.append(node)
        for line_property in connections:
            start_id, end_id = line_property['start_id'], line_property['end_id']
            print(start_id, end_id, self.nodes)
            start_port, end_port = self.find_port(start_id), self.find_port(end_id)
            mid_positions = line_property['mid_positions']
            mid_points = []
            for pos in mid_positions:
                mid_points.append(CustomMidPoint(pos=QPointF(*pos)))
            print(start_port, end_port, start_id, end_id)

            line = CustomLine(canvas=self, start_port=start_port, end_port=end_port, mid_points=mid_points)
            self.addItem(line)
            self.signal_item_dragged.connect(line.refresh)
            self.lines.append(line)
        print('loading finished!')

    def dump_flowchart(self, path=''):
        file ='./examples/flowchart_stat.json' if path == '' else path
        fc_info = {}
        connections = []
        nodes_dic = {}
        fc_info['nodes'] = nodes_dic
        fc_info['connections'] = connections
        for line in self.lines:
            line_properties = {}
            start_id = line.start_port.id
            end_id = line.end_port.id
            line_properties['start_id'] = start_id
            line_properties['end_id'] = end_id
            mid_positions = line.get_central_points_positions()
            line_properties['mid_positions'] = mid_positions
            connections.append(line_properties)
        print(self.nodes)
        for node in self.nodes:
            node_properties = {}
            node_properties['text'] = node.text
            node_properties['id'] = node.id
            node_properties['pos'] = node.get_pos()
            node_properties['icon'] = node.icon_path
            node_properties['content'] = node.content.dump()
            print(node_properties['content'])
            input_ports_dic = {}
            output_ports_dic = {}
            for port in node.input_ports:
                input_ports_dic[port.id] = {'id': port.id, 'pos': port.get_pos(), 'contents': {}, 'text': port.text}
            for port in node.output_ports:
                output_ports_dic[port.id] = {'id': port.id, 'pos': port.get_pos(), 'contents': {}, 'text': port.text}
            node_properties['input_ports'] = input_ports_dic
            node_properties['output_ports'] = output_ports_dic
            nodes_dic[node.id] = node_properties
        with open(file, 'w') as f:
            json.dump(fc_info, f, indent=4)
        pass

    def reset(self):
        self.clear()
        self.call_id_list: List = []
        self.lines: List[CustomLine] = []
        self.nodes: List['Node'] = []
        self.drawing_lines = False
        self.line_start_port = None
        self.line_start_point = None
        self.line_end_port = None
        self.node_index_to_execute = 0
        self.line = self.addLine(0, 0, 1, 1, QPen())


class PMGraphicsView(QGraphicsView):
    def __init__(self, parent: QWidget = None):
        super().__init__(parent)
        # self.setDragMode(QGraphicsView.ScrollHandDrag)  # 是否可以切换模式？

    def wheelEvent(self, event: 'QWheelEvent') -> None:
        """
        鼠标滚轮事件
        :param event:
        :return:
        """
        if event.modifiers() == Qt.ControlModifier:
            if event.angleDelta().y() > 0:
                self.scale(1.1, 1.1)
            else:
                self.scale(0.9, 0.9)


class PMFlowWidget(QWidget):
    def __init__(self,parent=None, path=''):
        self._path = path
        _translate = QCoreApplication.translate
        super().__init__()
        self.setObjectName("tab_flow")

        self.verticalLayout_6 = QVBoxLayout(self)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.widget_3 = QWidget(self)
        self.widget_3.setMinimumSize(QSize(0, 30))
        self.widget_3.setObjectName("widget_3")

        self.horizontal_layout = QHBoxLayout(self.widget_3)
        self.horizontal_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontal_layout.setSpacing(1)
        self.horizontal_layout.setObjectName("horizontalLayout_6")

        self.tool_button_run = QToolButton(self.widget_3)
        self.tool_button_run.setText('Run')
        self.tool_button_run.setIconSize(QSize(25, 25))
        self.tool_button_run.setObjectName("toolButton_4")

        self.horizontal_layout.addWidget(self.tool_button_run)

        self.tool_button_save = QToolButton(self.widget_3)
        self.tool_button_save.setText('Save')
        self.horizontal_layout.addWidget(self.tool_button_save)

        # self.tool_button_open = QToolButton(self.widget_3)
        # self.tool_button_open.setText('Open')
        # self.horizontal_layout.addWidget(self.tool_button_open)

        self.tool_button_run.clicked.connect(self.run)
        # self.tool_button_open.clicked.connect(self.open)
        self.tool_button_save.clicked.connect(self.save)

        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.horizontal_layout.addItem(spacerItem)
        self.verticalLayout_6.addWidget(self.widget_3)

        self.graphicsView = PMGraphicsView(self)

        self.graphicsView.setFrameShape(QFrame.NoFrame)
        self.graphicsView.setObjectName("graphicsView")
        self.verticalLayout_6.addWidget(self.graphicsView)

        self.scene = PMGraphicsScene(graphics_view=self.graphicsView, flow_widget=self)
        self.scene.setSceneRect(-1000, -1000, 2000, 2000)

        self.nodes: List[Node] = self.scene.nodes
        self.lines = self.scene.lines
        self.scene.load_flowchart(self._path)
        print(self.nodes)
        self.graphicsView.setScene(self.scene)
        print(self.scene.find_port(1))

    def reset(self):
        self.scene = PMGraphicsScene(graphics_view=self.graphicsView, flow_widget=self)
        self.scene.setSceneRect(-1000, -1000, 2000, 2000)

        self.nodes: List[Node] = self.scene.nodes
        self.lines = self.scene.lines
        self.load_flowchart()
        print(self.nodes)
        self.graphicsView.setScene(self.scene)
        print(self.find_port(1))

    def open(self):
        file_name, ext = QFileDialog.getOpenFileName(self, '选择文件', '', '流程图文件(*.pmcache *.json)')
        if file_name == '':
            return
        try:
            self.reset()
            self.load_flowchart(file_name)
        except:
            import traceback
            traceback.print_exc()
            pass
        # print(file_name)

    def save(self):
        self.scene.dump_flowchart(self._path)

    def run(self):
        call_id_list = self.scene.topo_sort()
        self.scene.call_id_list = call_id_list

        thread = QThread()
        # worker = MessageWork(self.server_obj)
        # worker.signal_socket_closed.connect(self.server_obj.remove_socket)
        # worker.client = client
        for node in self.nodes:
            node.reset()
            node.content.moveToThread(thread)
        worker = self.nodes[0].content
        # worker.server = self.server_obj
        worker.moveToThread(thread)
        thread.started.connect(worker._process)
        thread.start()
        self.worker = worker
        self.thread = thread

    def closeEvent(self, a0: 'QCloseEvent') -> None:
        self.scene.dump_flowchart(self._path)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    # graphics = PMFlowWidget(path=r'C:\Users\12957\Desktop\乡土中国.pmcache')
    # graphics.show()
    graphics2 = PMFlowWidget(path='./examples/flowchart_stat.json')
    graphics2.show()
    sys.exit(app.exec_())

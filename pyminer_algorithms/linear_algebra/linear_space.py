from typing import Union

import numpy


def linear_space(start: Union[float, numpy.ndarray], end: Union[float, numpy.ndarray], step: int):
    assert isinstance(start, (int, float, numpy.ndarray)), 'param 1 should be integer or array'
    assert isinstance(end, (int, float, numpy.ndarray)), 'param 2 should be integer or array'
    assert isinstance(step, int), 'step should be integer'
    try:
        return numpy.linspace(start, end, step)
    except:
        raise

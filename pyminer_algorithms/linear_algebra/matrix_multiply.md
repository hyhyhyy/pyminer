# `matrix_multiply`函数说明

## 用法

`matrix_multiply(a, b)`

## 说明

计算矩阵的乘积。
如果`a`是`m×p`矩阵，`b`是`p×n`矩阵，则计算结果`c`为`m×n`矩阵。 
其中，`c`的第`i`行第`j`列为`a`的第`i`行与`b`的第`j`列的内积。

目前仅支持两维数组，后续会支持将高维数组做为多个两维数组进行计算。
（恳请您能帮忙实现）

## 位置参数

1. `a`：矩阵乘法运算的第一个矩阵。
1. `b`：矩阵乘法运算的第二个矩阵。

## 返回值

矩阵乘法的计算结果。

## 备注

在`matlab`中，矩阵乘法可以直接用乘号`*`来表示。
在`numpy`中，对应的函数为`numpy.matmul`，也就是`matrix multiply`的简写。
在`numpy`中的乘号`*`表示数乘，或者按位乘，不是线性代数意义上的矩阵的乘法。

# 参考文献

1. [`matlab`中矩阵的乘法. MATLAB.][matlab]
1. [`numpy.matmul`帮助文档. Numpy.][numpy]

[matlab]: https://ww2.mathworks.cn/help/matlab/ref/mtimes.html
[numpy]: https://numpy.org/doc/stable/reference/generated/numpy.matmul.html

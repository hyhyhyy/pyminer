# `matrix_divide`函数说明

## 用法

`matrix_divide(a, b)`

## 说明

矩阵的除法。

在`pyminer-algorithms`中我们采用线性代数对除法的通常理解，即：

$Ax=B，x=A^{-1}B$

这在`MATLAB`当中也可以被称为左除，即对`inverse(A)`这一项在`B`的左侧。
如果您希望使用`MATLAB`中的右除，可以使用如下的格式得到同样的结果：

```python
from pyminer_algorithms import *
a = ones(3, 3)
b = ones(3, 3)
matrix_divide(b.T, a.T)
```

目前仅支持两维数组，后续会支持将高维数组作为多个两维数组进行计算。
（恳请您能帮忙实现）

## 位置参数

1. `a`：矩阵除法的分子。
1. `b`：这个参数也是用于示例。

## 返回值

这个函数返回了一个`array`对象，主要用于数据的进一步处理。
这是一个`2x2`的矩阵，第一列表示`x`坐标，第二列表示`y`坐标。

## 备注

`matlab`中定义了四种除法：
矩阵的按位左除`.\`、矩阵的按位右除`./`、矩阵的左除`\`、矩阵的右除`/`。
其中按位指的是对矩阵的每一个格子进行分别的计算，
而非线性代数意义上的矩阵的除法。

在`PyMiner`中，直接对矩阵进行`*`和`/`操作表示对矩阵进行按位乘法和按位除法。
矩阵的乘除法需要使用`matrix_multiply`和`matrix_divide`进行计算。
这主是要由于矩阵的乘法和除法并非一个通用的内容，
对于多维数组这两个功能实际上是无用的，
因此`pyminer-algorithms`不将其做为重要函数，故加了`matrix`前缀。

在`numpy`中是采用`numpy.solve`作为除法运算的，
这个函数表义不是太清晰，
个人认为不如`matrix_divide`清晰，故斗胆使用此命名，
如有建议或意见请提出。

# 参考文献

1. [`mrdivide`帮助文档. MATLAB.][matlab1]
1. [`mldivide`帮助文档. MATLAB.][matlab2]
1. [`numpy.solve`帮助文档. Numpy.][numpy]
1. [`scipy.solve`帮助文档. Numpy.][scipy]

[matlab1]: https://ww2.mathworks.cn/help/fixedpoint/ref/embedded.fi.mrdivide.html
[matlab2]: https://ww2.mathworks.cn/help/matlab/ref/mldivide.html
[numpy]: https://numpy.org/doc/stable/reference/generated/numpy.linalg.solve.html
[scipy]: https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.solve.html